<?php

  $items['school/%/data/cms/enrolment'] = array(
    'title' => t('Enrolment'),
    'description' => t('School Page'),
    'page callback' => 'school_cms_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '1',
  );

  $items['school/%/data/cms/seats'] = array(
    'title' => t('Seats'),
    'description' => t('School Page'),
    'page callback' => 'school_cms_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '2',
  );

  $items['school/%/data/cms/toilets'] = array(
    'title' => t('Toilets'),
    'description' => t('School Page'),
    'page callback' => 'school_cms_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '3',
  );

  $items['school/%/data/cms/textbooks'] = array(
    'title' => t('Textbooks'),
    'description' => t('School Page'),
    'page callback' => 'school_cms_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '4',
  );

  $items['school/%/data/cms/rooms'] = array(
    'title' => t('Rooms'),
    'description' => t('School Page'),
    'page callback' => 'school_cms_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '5',
  );

  $items['school/%/data/cms/personnel'] = array(
    'title' => t('Personnel'),
    'description' => t('School Page'),
    'page callback' => 'school_cms_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '6',
  );

  $items['school/%/data/cms/budget'] = array(
    'title' => "Budget ",
    'description' => t('School Page'),
    'page callback' => 'school_cms_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '7',
  );

  $items['school/%/data/cms/classes'] = array(
    'title' => t('Shifts & Classes'),
    'description' => t('School Page'),
    'page callback' => 'school_cms_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '8',
  );

  $items['school/%/data/cms/computers'] = array(
    'title' => t('Computer & Internet'),
    'description' => t('School Page'),
    'page callback' => 'school_cms_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '9',
  );


  $items['school/%/data/cms/test'] = array(
    'title' => t('Test Results'),
    'description' => t('School Page'),
    'page callback' => 'school_cms_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '10',
  );
  
  $items['school/%/data/cms/edit-data'] = array(
    'title' => t('Edit CMS Data'),
    'description' => t('School Page'),
    'page callback' => 'school_cms_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('edit any cms_school_data content'),
    'weight' => '11',
  );

/* depEd data menu */

$items['school/%/data/deped/enrolment'] = array(
    'title' => t('Enrolment'),
    'description' => t('School Page'),
    'page callback' => 'school_deped_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '1',
  );

  $items['school/%/data/deped/seats'] = array(
    'title' => t('Seats'),
    'description' => t('School Page'),
    'page callback' => 'school_deped_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '2',
  );

  $items['school/%/data/deped/toilets'] = array(
    'title' => t('Toilets'),
    'description' => t('School Page'),
    'page callback' => 'school_deped_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '3',
  );

  $items['school/%/data/deped/textbooks'] = array(
    'title' => t('Textbooks'),
    'description' => t('School Page'),
    'page callback' => 'school_deped_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '4',
  );

  $items['school/%/data/deped/rooms'] = array(
    'title' => t('Rooms'),
    'description' => t('School Page'),
    'page callback' => 'school_deped_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '5',
  );

  $items['school/%/data/deped/personnel'] = array(
    'title' => t('Personnel'),
    'description' => t('School Page'),
    'page callback' => 'school_deped_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '6',
  );

  $items['school/%/data/deped/budget'] = array(
    'title' => t('Budget'),
    'description' => t('School Page'),
    'page callback' => 'school_deped_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '7',
  );

  $items['school/%/data/deped/classes'] = array(
    'title' => t('Shifts & Classes'),
    'description' => t('School Page'),
    'page callback' => 'school_deped_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '8',
  );

  $items['school/%/data/deped/computers'] = array(
    'title' => t('Computer & Internet'),
    'description' => t('School Page'),
    'page callback' => 'school_deped_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '9',
  );


  $items['school/%/data/deped/test'] = array(
    'title' => t('Test Results'),
    'description' => t('School Page'),
    'page callback' => 'school_deped_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('access content'),
    'weight' => '10',
  );
  
  $items['school/%/data/deped/edit-data'] = array(
    'title' => t('Edit DepEd Data'),
    'description' => t('School Page'),
    'page callback' => 'school_info_edit_data',
    'page arguments' => array(1,3,4),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cms_lib.inc',
    'access arguments' => array('edit any school_data_deped content'),
    'weight' => '11',
  );
  


?>
