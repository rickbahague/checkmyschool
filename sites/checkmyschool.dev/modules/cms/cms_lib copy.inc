 <?php
 
function school_page($id) {
  $html = l('Enrolment','enrolment') . '|'. l('Seats','seats');
  $node = node_load($id);
  $output = field_view_field('node', $node, 'field_deped_enrolment', array('label' => 'hidden'));
  return $output;
}

function school_variance($id) {
  $output = t('No data found.');
  return '<div id="no-data">'.$output.'</div>';
}

function school_needs($id) {
  $output = t('No data found.');
  return '<div id="no-data">'.$output.'</div>';
}

function school_cms_data ($school_id, $source, $data = null) {

  //$result = db_query("select entity_id as nid from field_data_field_school_ref, node
  //                   where node.nid = field_data_field_school_ref.entity_id and
  //                   field_school_ref_target_id = $school_id and bundle = 'school_data_cms'");



  $result = db_query("select entity_id, nid, node.title from field_data_field_school_ref, node 
                     where 
                     field_school_ref_target_id = $school_id 
                     and bundle = 'cms_school_data'
                     and node.nid = field_data_field_school_ref.entity_id
                     order by created desc limit 1");

  foreach($result as $row){
    $nid = $row->nid;
  }
  
  if(!empty($nid)) {
    $node = node_load($nid);
  } else {
    $output = "No data found";
    return '<div id="no-data">'.$output.'</div>';
  }
    
  if($data == null) {
    $data = 'enrolment';
  }
 
  if($data == 'enrolment') {
    $output = field_view_field('node', $node, 'field_cms_enrolment', array('label' => 'hidden'));
    return $output;
  }
  
  if($data == 'seats') {
    $output = field_view_field('node', $node, 'field_cms_seats', array('label' => 'hidden'));
    return $output;
  }

  if($data == 'toilets') {
    $output = field_view_field('node', $node, 'field_cms_toilets', array('label' => 'hidden'));
    return $output;
  }
  
  if($data == 'textbooks') {
    $output = field_view_field('node', $node, 'field_cms_textbooks', array('label' => 'hidden'));
    return $output;
  }

  if($data == 'rooms') {
    $output = field_view_field('node', $node, 'field_cms_rooms', array('label' => 'hidden'));
    return $output;
  }
  
  if($data == 'personnel') {
    $output = field_view_field('node', $node, 'field_cms_personnel', array('label' => 'hidden'));
    return $output;
  }
  
  if($data == 'budget') {
    $output = field_view_field('node', $node, 'field_cms_budget', array('label' => 'hidden'));
    return $output;
  }

  if($data == 'classes') {
    $output = field_view_field('node', $node, 'field_cms_shifts_classes', array('label' => 'hidden'));
    return $output;
  }
  
  if($data == 'computers') {
    $output = field_view_field('node', $node, 'field_cms_computerbudget', array('label' => 'hidden'));
    return $output;
  }

  if($data == 'test') {
    $output = field_view_field('node', $node, 'field_cms_test_results', array('label' => 'hidden'));
    return $output;
  }
  
  if($data == 'edit-data'){
    drupal_goto('node/'.$nid.'/edit');
  }
}

function school_deped_data ($school_id, $source, $data = null) {
  
  $result = db_query("select entity_id, nid, node.title from field_data_field_school_ref, node 
                     where 
                     field_school_ref_target_id = $school_id 
                     and bundle = 'school_data_deped' 
                     and node.nid = field_data_field_school_ref.entity_id
                     order by created desc limit 1");



  foreach($result as $row){
    $nid = $row->nid;
  }

  if(!empty($nid)) {
    $node = node_load($nid);
  } else {
    $output = "No data found";
    return '<div id="no-data">'.$output.'</div>';
  }

  if($data == null) {
    $data = 'enrolment';
  }
 
 
  if($data == 'enrolment') {
    $output = field_view_field('node', $node, 'field_deped_enrolment', array('label' => 'hidden'));  
    return $output;
  }
  
  if($data == 'seats') {
    $output = field_view_field('node', $node, 'field_deped_seats', array('label' => 'hidden'));
    return $output;
  }

  if($data == 'toilets') {
    $output = field_view_field('node', $node, 'field_deped_toilets', array('label' => 'hidden'));
    return $output;
  }
  
  if($data == 'textbooks') {
    $output = field_view_field('node', $node, 'field_deped_textbooks', array('label' => 'hidden'));
    return $output;
  }

  if($data == 'rooms') {
    $output = field_view_field('node', $node, 'field_deped_rooms', array('label' => 'hidden'));
    return $output;
  }
  
  if($data == 'personnel') {
    $output = field_view_field('node', $node, 'field_deped_personnel', array('label' => 'hidden'));
    return $output;
  }
  
  if($data == 'budget') {
    $output = field_view_field('node', $node, 'field_deped_budget', array('label' => 'hidden'));
    return $output;
  }

  if($data == 'classes') {
    $output = field_view_field('node', $node, 'field_deped_shifts_classes', array('label' => 'hidden'));
    return $output;
  }
  if($data == 'computers') {
    $output = field_view_field('node', $node, 'field_deped_computer_budget', array('label' => 'hidden'));
    return $output;
  }

  if($data == 'test') {
    $output = field_view_field('node', $node, 'field_deped_test_results', array('label' => 'hidden'));
    return $output;
  }
/*
  if($data == 'edit-data') {
    school_info_edit_data($nid);
  }
*/  
  if($data == 'edit-data'){
    drupal_goto('node/'.$nid.'/edit');
  }

/*  
  if(!empty($output)) {
    $output .= "<div id='school-deped-data'>" .$output. "</div>";
    return $output;   
  } else {
    return t("No data found."); 
  }
*/

}

function cms_sms(){
  
  // $_SERVER['REMOTE_ADDR']=='125.5.124.146' && !empty($_REQUEST['pid'])
  if (!empty($_REQUEST['pid'])) {
    $url_path = $_REQUEST;
   // $sms_data['telco'] = $telco;
    $sms_data['pid'] = $url_path['pid'];
    $sms_data['cel'] = $url_path['cel'];
    $sms_data['msg'] = $url_path['msg'];
    $sms_data['created'] = strtotime(date('Y-m-d'));
    
    // Store data on a MySQL table...
    db_insert('cms_sms')->fields($sms_data)->execute();
    //var_dump($_REQUEST);
    //http://api.mymegamobile.com/<filename>?pid=123456789-12345- 12345&cel=INT&msg=XXX&tcs=X
    /*
    $reply = "Thank you for contributing.";
    
    $data = 'pid='.$sms_data.'&cel='.$sms_data['cel'].'&msq='.$reply;
    $options = array(
                'method' => 'GET',
                'data' => $data,
                'timeout' => 15,
                'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
        );
    
    $result = drupal_http_request('http://api.mymegamobile.com/api/checkmyschool', $options, 15);
    */
    //var_dump($result);
    
    echo "Data inserted";
    
  } else {
    echo "Invalid data.";
  // var_dump($_REQUEST);    
  }
  
}

function cms_update(){
  
  $query = "select nid from node where type = 'school' and nid between 1893 and 2000";
  $results = db_query($query);
  
  $count = 0;
 /* 
  foreach($results as $row){
  
 // $nid = 1388; // The node to update

  $node = node_load($row->nid); // ...where $nid is the node id

  //$temp = explode(',',$node->title);

  //$node->title = $node->title;
  
  $node->title = strtoupper($node->title.', '.$node->field_school_district['und']['0']['value'].', '.$node->field_school_division['und']['0']['value']);

  //$node->body[$node->language][0]['value']   = "And a new body text, too.";

  
  if($node = node_submit($node)) { // Prepare node for saving
    node_save($node);
 //   echo "Node with nid " . $node->nid . " updated!\n";
  }
 
    $count = $count + 1;
  }
  var_dump($count);
 */
}


function school_info_edit_data($school_id, $source, $data = null){
  
  if($source == 'cms'){
    $result = db_query("select entity_id, nid, node.title from field_data_field_school_ref, node 
                     where 
                     field_school_ref_target_id = $school_id 
                     and bundle = 'cms_school_data'
                     and year((date(title))) = year(now())
                     and node.nid = field_data_field_school_ref.entity_id");
  }elseif($source == 'deped'){
    $result = db_query("select entity_id, nid, node.title from field_data_field_school_ref, node 
                     where 
                     field_school_ref_target_id = $school_id 
                     and bundle = 'school_data_deped' 
                     and year((date(title))) = year(now())
                     and node.nid = field_data_field_school_ref.entity_id");
  }
  
  foreach($result as $row){
    $nid = $row->nid;
  }
  
  if(!empty($nid)) {
    $node = node_load($nid);
    drupal_goto('/node/'.$nid.'/edit');
  } else {
    $output = "No data found.hello.$nid";
    return '<div id="no-data">'.$output.'</div>';
  }
  
}

function cms_sms_all(){

  $query = "select msg, cel, from_unixtime(created) as c from (select msg, cel, created from cms_sms union select message as msg, from_number as cel, sms_timestamp as created from tfpw_sms) as a where msg!='' order by from_unixtime(created) asc";

  //$query = 'select * from cms_sms order by created desc';
  $result = db_query($query);
  $html = '<ul>';
  foreach($result as $row){
    $html .= '<li> '.$row->msg.' ';
    $cel = substr_replace($row->cel,'*',0, 8);
    $html .= ' - <em>From 0'.$cel.'</em>';
    $html .= ' on '.$row->c;
    $html .= '</li>';
  };
  $html .= '</ul>'; 
  return '<div id="sms-page">'.$html.'</div>';
}
