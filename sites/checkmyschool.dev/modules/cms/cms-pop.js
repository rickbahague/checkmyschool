$(document).ready(function() {

	$('form[name="resolve-form"]').submit(function(event) {
			event.preventDefault();

			$('form[name="resolve-form"]').hide();
			
			nid = $('#nid').val();
			feedback_tag = $("#feedback_tag").val();
			feedback = $("#feedback").val();
			firstname = $("#firstname").val();
			lastname = $("#lastname").val();
			console.log(nid);

			$.ajax({
				url: '/cms/feedback/send/submit/nid='+nid+'/feedback_tag='+feedback_tag+'/feedback='+feedback+'/firstname='+firstname+'/lastname='+lastname,
				data: {'nid':nid, 'feedback_tag':feedback_tag, 'feedback':feedback, 'firstname':firstname, 'lastname':lastname},
				type: 'POST',
				async: false,
				success: function(data){
					class_add = '<p div class="response-message">' + data + '</p>';
					$(class_add).insertAfter('#resolve-form')
					console.log(data);	
				}
			});
	});

	

});

