 <?php


function view_volunteers_data($uid){

  $userdata = user_load($uid);

  $tempFname = field_get_items('user', user_load($uid), 'field_user_firstname');
  $firstname = $tempFname[0]['value'];

  $tempLname = field_get_items('user', user_load($uid), 'field_user_lastname');
  $lastname = $tempLname[0]['value'];


  $html = '<div id="volunteer-data-info">';
  $html .= '<h3>Volunteer Profile</h3>';
  if(!empty($userdata->picture->filename)){
    $html .= '<div id="volunteer-photo">
                  <img width="200px" src="/sites/checkmyschool.dev/files/pictures/'.$userdata->picture->filename.'">
            </div>';
  }

  $html = '<div id="volunteer-profile">
              <p class="name">Name: '.$firstname.' '.$lastname.'</p>
              <p class="nickname">Nickname:</p>
              <p class="District">District:</p>
              <p class="Division">Division:</p>
              <p class="School">School:</p>
              <p class="Contact Number">Contact Num:</p>
              <p class="Email">Email</p>
            </div>
          </div>';


  return $html;

}
 
 function view_volunteers($uid = null){

    $volunteers = array();
    $path = drupal_get_path('module','cms');
    drupal_add_js($path.'/js/view_volunteers.js');
    drupal_add_css($path.'/css/cms_volunteers.css');

    $html = '<div id="top-bar">'; 
    $html .= '<span>Search volunteers using filters below.</span>';
    $html .= '<form id="form-volunteers" name="form-volunteers" action="" method="post">';
    $html .= '<select name="volregion" id="volregion">'.view_volunteers_region().'</select>';
    $html .= '<select name="voldivision" id="voldivision"><option value="">--Select Division--</option></select>';
    $html .= '<select name="voldistrict" id="voldistrict"><option value="">--Select District--</option></select>';
    $html .= '<input id ="submit-volunteers" type="submit" name="submit" value="GO">';
    $html .= '</form>';
    $html .= '</div>';
    $loc_crumbs = '';

    if(!empty($_POST)){

      $volunteers = view_volunteers_process($_POST['voldivision'], $_POST['voldistrict'], $_POST['volregion']);

      $volunteer_data = view_volunteers_data('29'); 

      $loc_crumbs = $_POST['volregion'].' >> '.$_POST['voldivision'].' >> '.$_POST['voldistrict'];

    }elseif(empty($_POST) && !empty($uid)){

      $query_nid = "select field_volunteer_school_target_id as nid from field_data_field_volunteer_school where entity_id =:uid ";
      $result_nid = db_query($query_nid, array(':uid' => $uid))->fetchAssoc();
      $sch_id = $result_nid['nid'];

      $query_schools = "select region,district,division from cms_schools_collected where nid = $sch_id";
      $result = db_query($query_schools)->fetchAssoc();
      
      $volunteers = view_volunteers_process($result['division'],$result['district'], $result['region']);

      $volunteer_data = view_volunteers_data($uid);  

      $loc_crumbs = $result['region'].' >> '.$result['division'].' >> '.$result['district'];    

    }elseif(empty($_POST) && empty($uid)){

      $volunteers = 'Search';
      $volunteer_data = view_volunteers_data('29');
      $loc_crumbs = '';

    }

    $html .= '<div id="volunteer-page">';
    $html .= '<ul id="list-volunteers">';
    $html .= '<h3>List of Volunteers</h3>';
    $html .= '<div id="loc-crumbs">'.$loc_crumbs.'</div>';

    if($volunteers !== 'None found.' && $volunteers !== 'Search'){

      foreach($volunteers as $volunteer){

        $html .= '<li id="item-volunteer">'. $volunteer .'</li>';

      }
    
    }elseif($volunteers == 'None found.'){
      
      $html .= '<li id="item-volunteer"><b>None found.</b></li>';

    }elseif($volunteers == 'Search'){
      
       $html .= '<li id="item-volunteer"><b>Use location filters above.</b></li>';
    
    }
    
    $html .= '</ul>';
    $html .= '</div>';
    return $html;
}

/*
get school data

select field_school_division_value as division,
field_school_district_value as district,
field_school_region_value as region,
field_school_id_value as school_id,
field_school_name_now_value as school_name,
node.title as title,
node.nid as nid
from field_data_field_school_division, 
field_data_field_school_district,
field_data_field_school_region,
field_data_field_school_id,
field_data_field_school_name_now,
node
where 
field_data_field_school_division.entity_id = node.nid and
field_data_field_school_district.entity_id = node.nid and
field_data_field_school_id.entity_id = node.nid and
field_data_field_school_name_now.entity_id = node.nid and
field_data_field_school_region.entity_id = node.nid;
*/

function view_volunteers_process($division, $district, $region){

  $query_schools = "select nid from cms_schools_collected where region = :region and district = :district and division = :division";
  $results_schools = db_query($query_schools,array(':region'=> $region, ':district' => $district, ':division' => $division));

  $count = 0;

  if($results_schools->rowCount() > 0){

    $query = "select field_volunteer_school_target_id, entity_id from field_data_field_volunteer_school where field_volunteer_school_target_id = :nid";

    $volunteers = array();

    foreach($results_schools as $row){

      $results_volunteers = db_query($query,array(':nid' => $row->nid));

      if($results_volunteers->rowCount() > 0){

         //var_dump($results_volunteers->rowCount());

         foreach($results_volunteers as $row_volunteer){

            //print_r($row_volunteer);

            $tempFname = field_get_items('user', user_load($row_volunteer->entity_id), 'field_user_firstname');
            $firstname = $tempFname[0]['value'];

            $tempSname = field_get_items('user', user_load($row_volunteer->entity_id), 'field_user_lastname');
            $lastname = $tempSname[0]['value'];

            //$options['attributes'] = array('class'=>'volunteer-name');

            //$volunteers[] = l($firstname. ' '.$lastname,'cms/volunteers/'.$row_volunteer->entity_id,$options);

            $volunteers[] = '<a class ="volunteer-name" href="/cms/volunteers/'.$row_volunteer->entity_id.'">'.$firstname.' '.$lastname.'</a>';

            //print $lastname.$firstname;


         }
      }

    }
    
  }else{
    $volunteers = "None found.";
  }

  return $volunteers;
}



function view_volunteers_region(){

  $query = "select region from cms_schools_collected group by region";
  $html = "";
  $result = db_query($query);
  $html .="<option value=''>--Select Region--</option>";

  foreach($result as $row){
    $html.= '<option value="'.$row->region.'">'.htmlentities($row->region).'</option>';
  }

  return $html;

}

function view_volunteers_district_start(){

  $division = $_POST['division'];
  echo "<option value='''' selected='selected'>--Select District--</option>";
}

function view_volunteers_district(){

  $division = $_POST['division'];
  //$region = $_POST['region'];

  $query = "select district as district_name from cms_schools_collected where division = :division group by district ";

  $result = db_query($query,array(':division' => $division));


  $html = "";

  foreach($result as $row){
    $html .= "<option value='".$row->district_name."'>".$row->district_name."</option>";
  }
  $html .= "<option value='' selected='selected'>--Select District--</option>"; 

  echo $html;

}

function view_volunteers_division(){

  $region = $_POST['region'];

  $query = "select division as division_name from cms_schools_collected where region = :region group by division";

  $result = db_query($query,array(':region' => $region));

  $html = "";

  foreach($result as $row){
    $html .= "<option value='".$row->division_name."'>".$row->division_name."</option>";
  }

  $html .= "<option value='' selected='selected'>--Select Division--</option>";
  $html .= $row->division_name;

  echo $html;

}