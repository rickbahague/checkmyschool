<?php
/**
 * @file
 * school_context.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function school_context_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'school_info_page_search';
  $context->description = 'School-Info-Page-Search';
  $context->tag = 'School-Pages-Context';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'search/node' => 'search/node',
        'search/node/*' => 'search/node/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'boxes-box_school_info_search_top_image' => array(
          'module' => 'boxes',
          'delta' => 'box_school_info_search_top_image',
          'region' => 'content',
          'weight' => '-58',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-57',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'schoolinfo_page_template',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('School-Info-Page-Search');
  t('School-Pages-Context');
  $export['school_info_page_search'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'school_page_context';
  $context->description = '';
  $context->tag = 'School-Pages-Context';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'school/*' => 'school/*',
        '~school/map' => '~school/map',
        '~school/data/visualize' => '~school/data/visualize',
        '~school/map/help' => '~school/map/help',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'cck_blocks-field_school_logo' => array(
          'module' => 'cck_blocks',
          'delta' => 'field_school_logo',
          'region' => 'content',
          'weight' => '-10',
        ),
        'cck_blocks-field_school_id' => array(
          'module' => 'cck_blocks',
          'delta' => 'field_school_id',
          'region' => 'content',
          'weight' => '-9',
        ),
        'cck_blocks-field_school_barangay' => array(
          'module' => 'cck_blocks',
          'delta' => 'field_school_barangay',
          'region' => 'content',
          'weight' => '-8',
        ),
        'cck_blocks-field_school_street' => array(
          'module' => 'cck_blocks',
          'delta' => 'field_school_street',
          'region' => 'content',
          'weight' => '-7',
        ),
        'cck_blocks-field_school_district' => array(
          'module' => 'cck_blocks',
          'delta' => 'field_school_district',
          'region' => 'content',
          'weight' => '-6',
        ),
        'cck_blocks-field_school_division' => array(
          'module' => 'cck_blocks',
          'delta' => 'field_school_division',
          'region' => 'content',
          'weight' => '-5',
        ),
        'cck_blocks-field_school_region' => array(
          'module' => 'cck_blocks',
          'delta' => 'field_school_region',
          'region' => 'content',
          'weight' => '-4',
        ),
        'cck_blocks-field_school_head' => array(
          'module' => 'cck_blocks',
          'delta' => 'field_school_head',
          'region' => 'content',
          'weight' => '-3',
        ),
        'cck_blocks-field_school_designation' => array(
          'module' => 'cck_blocks',
          'delta' => 'field_school_designation',
          'region' => 'content',
          'weight' => '-2',
        ),
        'cck_blocks-field_school_telno' => array(
          'module' => 'cck_blocks',
          'delta' => 'field_school_telno',
          'region' => 'content',
          'weight' => '-1',
        ),
        'cck_blocks-field_school_fax' => array(
          'module' => 'cck_blocks',
          'delta' => 'field_school_fax',
          'region' => 'content',
          'weight' => '0',
        ),
        'cck_blocks-field_school_email' => array(
          'module' => 'cck_blocks',
          'delta' => 'field_school_email',
          'region' => 'content',
          'weight' => '1',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '2',
        ),
        'boxes-school_info_bottom_buttons' => array(
          'module' => 'boxes',
          'delta' => 'school_info_bottom_buttons',
          'region' => 'content',
          'weight' => '3',
        ),
        'cms-school-page-menu' => array(
          'module' => 'cms',
          'delta' => 'school-page-menu',
          'region' => 'preface_second',
          'weight' => '-10',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => '2013_node_template',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('School-Pages-Context');
  $export['school_page_context'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'school_pages_context';
  $context->description = 'School-Pages-Context';
  $context->tag = 'School-Pages-Context';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'school/map' => 'school/map',
        'school/data/visualize' => 'school/data/visualize',
        'school/pages' => 'school/pages',
        'summary/data/national' => 'summary/data/national',
        'school/map/help' => 'school/map/help',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-56',
        ),
        'boxes-school_info_bottom_buttons' => array(
          'module' => 'boxes',
          'delta' => 'school_info_bottom_buttons',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'home_page_template',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('School-Pages-Context');
  $export['school_pages_context'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'school_photo_page_context';
  $context->description = '';
  $context->tag = 'School-Pages-Context';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'school/*/video' => 'school/*/video',
        'school/*/partner' => 'school/*/partner',
        'school/*/documents' => 'school/*/documents',
        'school/*/photo' => 'school/*/photo',
        'school/*/data/*' => 'school/*/data/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'boxes-school_info_bottom_buttons' => array(
          'module' => 'boxes',
          'delta' => 'school_info_bottom_buttons',
          'region' => 'content',
          'weight' => '-9',
        ),
        'cms-school-page-menu' => array(
          'module' => 'cms',
          'delta' => 'school-page-menu',
          'region' => 'preface_second',
          'weight' => '-10',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => '2013_node_template',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('School-Pages-Context');
  $export['school_photo_page_context'] = $context;

  return $export;
}
