<?php
/**
 * @file
 * school_context.box.inc
 */

/**
 * Implements hook_default_box().
 */
function school_context_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'box_school_info_search_top_image';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Box-School-Info-Search-Top-Image';
  $box->options = array(
    'body' => array(
      'value' => '<p><img src="/sites/checkmyschool.dev/files/site-images/Search-Icon.png"></p>',
      'format' => 'full_html',
    ),
    'additional_classes' => 'Box-School-Info-Search-Top-Image',
  );
  $export['box_school_info_search_top_image'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'school_info_bottom_buttons';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Box-School-Info-Bottom-Buttons';
  $box->options = array(
    'body' => array(
      'value' => '<ul><li><a href="/school/pages"><img src="/sites/checkmyschool.dev/files/site-images/School-Info-SchoolPages-Button.png"></a></li><li><a href="/school/map"><img src="/sites/checkmyschool.dev/files/site-images/School-Info-SchoolMap-Button.png"></a><li><a href="/gallery/photos"><img src="/sites/checkmyschool.dev/files/site-images/School-Info-Gallery-Button.png"></a></li><li><a href="/archive"><img src="/sites/checkmyschool.dev/files/site-images/School-Info-Archive-Button.png"></a></li><li><a href="/school/data/visualize"><img src="/sites/checkmyschool.dev/files/site-images/School-Info-Data-Vis-Button.png"></a></li></ul>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['school_info_bottom_buttons'] = $box;

  return $export;
}
