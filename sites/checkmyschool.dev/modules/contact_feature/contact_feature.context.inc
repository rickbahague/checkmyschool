<?php
/**
 * @file
 * contact_feature.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function contact_feature_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'contact_page_context';
  $context->description = 'About-Us-Contact-Page-Context';
  $context->tag = 'Static-Pages';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'contact' => 'contact',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'boxes-box_contact_us_info' => array(
          'module' => 'boxes',
          'delta' => 'box_contact_us_info',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'clone_of_2013_node_template',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('About-Us-Contact-Page-Context');
  t('Static-Pages');
  $export['contact_page_context'] = $context;

  return $export;
}
