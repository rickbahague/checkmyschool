<?php
/**
 * @file
 * contact_feature.box.inc
 */

/**
 * Implements hook_default_box().
 */
function contact_feature_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'box_contact_us_info';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Box-Contact-Info';
  $box->options = array(
    'body' => array(
      'value' => '<div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><div>&nbsp;</div><div><span style="font-size:16px;">ANSA-EAP Office, 3rd Floor</span></div><div><span style="font-size:16px;">Mansi Bldg. 337</span></div><div><span style="font-size:16px;">Loyola Heights, Katipunan Avenue</span></div><div><span style="font-size:16px;">Quezon City, Metropolitan Manila</span></div><div><span style="font-size:16px;">Philippines 1108</span></div><div>&nbsp;</div><div><div><span style="font-size:16px;">Telephone:&nbsp;</span></div><div><span style="font-size:16px;">(+632)(426-0185)</span></div><div>&nbsp;</div><div><span style="font-size:16px;">Email:&nbsp;</span></div><div><span style="font-size:16px;">feedback@checkmyschool.org</span></div></div><p>&nbsp;</p>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['box_contact_us_info'] = $box;

  return $export;
}
