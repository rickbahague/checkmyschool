<?php
/**
 * @file
 * school_data_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function school_data_feature_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function school_data_feature_node_info() {
  $items = array(
    'cms_validated_data' => array(
      'name' => t('CMS Validated Data'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
