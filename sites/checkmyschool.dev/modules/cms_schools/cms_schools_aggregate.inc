<?php 

$html .= ' 
	    <div id="top-bar">
	    <span></span>
	    <form id="form-volunteers" name="form-volunteers" action="" method="post">
	    <select name="volregion" id="volregion">'.view_aggregate_region().'</select>
	    <select name="voldivision" id="voldivision"><option value="">--Select Division--</option></select>
	    <select name="voldistrict" id="voldistrict"><option value="">--Select District--</option></select>
	    <input id ="submit-volunteers" type="submit" name="submit" value="GO">
	    </form>
	    </div>
		<div>&nbsp;</div>	
		<span>NATIONAL</span>
		<table>
		<tr><td colspan="4">A. ENROLMENT</td></tr>
		<tr><td></td><td>MALE</td><td>FEMALE</td><td>TOTAL</td></tr>
		<tr><td>Total Enrolment</td><td></td><td></td><td></td></tr>
		<tr><td>CCT/4Ps Recipient</td><td></td><td></td><td></td></tr>
		<tr><td>Transferees In</td><td></td><td></td><td></td></tr>
		<tr><td>Transferees Out</td><td></td><td></td><td></td></tr>
		<tr><td>Dropouts</td><td></td><td></td><td></td></tr>
		<tr><td></td><td></td><td></td><td></td></tr>
		<tr><td>Number of shifts</td><td colspan="3"></td></tr>
		<tr><td>Count of monograde classes</td><td colspan="3"></tr>
		<tr><td>Count of multigrade classes</td><td colspan="3"></tr>
		</table>


		<table>
		<tr><td colspan="4">B. SEATS</td></tr>
		<tr><td></td><td>QUANTITY</td><td>SEATING CAPACITY</td><td></td></tr>
		<tr><td>Armchair/Chair</td><td></td><td></td><td></td></tr>
		<tr><td>Desks</td><td></td><td></td><td></td></tr>
		<tr><td>Other Types</td><td></td><td></td><td></td></tr>
		<tr><td>TOTAL SEATING CAPACITY</td><td></td><td></td><td></td></tr>
		</table>

		<table>
		<tr><td colspan="4">C. TOILETS</td></tr>
		<tr><td></td><td>QUANTITY</td><td></td><td></td></tr>
		<tr><td>Toilet for Boys</td><td></td><td></td><td></td></tr>
		<tr><td>Toilet for Girls</td><td></td><td></td><td></td></tr>
		<tr><td>Urinals/Through</td><td></td><td></td><td></td></tr>
		</table>

		<table>
		<tr><td colspan="4">D. TEXTBOOKS</td></tr>
		<tr><td></td><td>TOTAL</td><td></td><td></td></tr>
		<tr><td>Filipino</td><td></td><td></td><td></td></tr>
		<tr><td>English</td><td></td><td></td><td></td></tr>
		<tr><td>Math</td><td></td><td></td><td></td></tr>
		<tr><td>Makabayan</td><td></td><td></td><td></td></tr>
		<tr><td>Science</td><td></td><td></td><td></td></tr>
		</table>


		';
