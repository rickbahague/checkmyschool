<?php
/**
 * @file
 * response_to_feedback.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function response_to_feedback_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function response_to_feedback_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function response_to_feedback_image_default_styles() {
  $styles = array();

  // Exported image style: feedback-carousel.
  $styles['feedback-carousel'] = array(
    'name' => 'feedback-carousel',
    'label' => 'feedback-carousel',
    'effects' => array(
      13 => array(
        'label' => 'Resize',
        'help' => 'Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.',
        'effect callback' => 'image_resize_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_resize',
        'data' => array(
          'width' => '200',
          'height' => '150',
        ),
        'weight' => '1',
      ),
    ),
  );

  // Exported image style: see-feedback-display.
  $styles['see-feedback-display'] = array(
    'name' => 'see-feedback-display',
    'label' => 'see-feedback-display',
    'effects' => array(
      14 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '450',
          'height' => '400',
        ),
        'weight' => '1',
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function response_to_feedback_node_info() {
  $items = array(
    'response' => array(
      'name' => t('Response'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
