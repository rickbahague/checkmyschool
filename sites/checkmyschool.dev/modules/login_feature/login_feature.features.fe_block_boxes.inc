<?php
/**
 * @file
 * login_feature.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function login_feature_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Box-Sign-In';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'box_sign_in';
  $fe_block_boxes->body = '<p><a href="/user/register">Sign-up</a> | <a href="/user">Sign-in</a> or login with&nbsp;</p>';

  $export['box_sign_in'] = $fe_block_boxes;

  return $export;
}
