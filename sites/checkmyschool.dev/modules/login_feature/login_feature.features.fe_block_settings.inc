<?php
/**
 * @file
 * login_feature.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function login_feature_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-box_sign_in'] = array(
    'cache' => -1,
    'custom' => '0',
    'machine_name' => 'box_sign_in',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(
      'anonymous user' => '1',
    ),
    'themes' => array(
      'checkmyschool2013' => array(
        'region' => 'user_first',
        'status' => '1',
        'theme' => 'checkmyschool2013',
        'weight' => '-75',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  return $export;
}
