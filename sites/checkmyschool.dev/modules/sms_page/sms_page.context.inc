<?php
/**
 * @file
 * sms_page.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function sms_page_context_default_contexts() {
  $export = array();

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sms-page-context';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'sms/all' => 'sms/all',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-52',
        ),
        'block-5' => array(
          'module' => 'block',
          'delta' => 5,
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-main_page_views-block' => array(
          'module' => 'views',
          'delta' => 'main_page_views-block',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'views-main_page_views-block_1' => array(
          'module' => 'views',
          'delta' => 'main_page_views-block_1',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'views-main_page_views-block_videos' => array(
          'module' => 'views',
          'delta' => 'main_page_views-block_videos',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
        'views-main_page_views-block_2' => array(
          'module' => 'views',
          'delta' => 'main_page_views-block_2',
          'region' => 'sidebar_second',
          'weight' => '-6',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'node_template',
    ),
  );
  $context->condition_mode = 0;
  $export['sms-page-context'] = $context;

  return $export;
}
