<?php

    $items['cms/feedback/survey'] = array(
        'title' => t('SURVEY'),
        'description' => t('View feedbacks'),
        'page callback' => 'recent_survey',
        'type' => MENU_NORMAL_ITEM,
        'access arguments' => array('access content'),
        'file' => 'cms_feedback.inc',
        'menu' => 'main-menu',
    );


    $items['feedback/send'] = array(
        'title' => t('SEND FEEDBACK'),
        'description' => t('Send Feedback'),
        'page callback' => 'feedback_send',
        'type' => MENU_NORMAL_ITEM,
        'access arguments' => array('access content'),
        'file' => 'cms_feedback.inc',
        'menu' => 'main-menu',
    );

    $items['cms/feedback/summary'] = array(
        'title' => t('FEEDBACK'),
        'description' => t('Summary of feedbacks'),
        'page callback' => 'feedback_summary',
        'type' => MENU_NORMAL_ITEM,
        'access arguments' => array('access content'),
        'file' => 'cms_feedback.inc',
        'menu' => 'main-menu',
    );

    $items['cms/feedback/%'] = array(
        'title' => t('Feedback'),
        'description' => t('Feedbacks'),
        'page callback' => 'feedback_view',
        'type' => MENU_NORMAL_ITEM,
        'access arguments' => array('allow feedback access'),
        'file' => 'cms_feedback.inc',
    );

    $items['cms/feedback/new/tracker'] = array(
        'title' => t('SEE FEEDBACK'),
        'description' => t('View feedbacks'),
        'page callback' => 'feedback_front',
        'page arguments' => array(2),
        'type' => MENU_NORMAL_ITEM,
        'access arguments' => array('access content'),
        'file' => 'cms_feedback.inc',
        'menu' => 'main-menu',
    );

    $items['cms/feedback/responded/tracker'] = array(
        'title' => t('RESPONDED FEEDBACK'),
        'description' => t('View feedbacks'),
        'page callback' => 'feedback_front',
        'page arguments' => array(2),
        'type' => MENU_NORMAL_ITEM,
        'access arguments' => array('access content'),
        'file' => 'cms_feedback.inc',
        'menu' => 'main-menu',
    );

    $items['cms/feedback/resolved/tracker'] = array(
        'title' => t('RESOLVED FEEDBACK'),
        'description' => t('View feedbacks'),
        'page callback' => 'feedback_front',
        'page arguments' => array(2),
        'type' => MENU_NORMAL_ITEM,
        'access arguments' => array('access content'),
        'file' => 'cms_feedback.inc',
        'menu' => 'main-menu',
    );


    $items['cms/feedback/view'] = array(
        'page callback' => 'feedback_view',
        'type' => MENU_CALLBACK,
        'access arguments' => array('allow feedback access'),
        'file' => 'cms_feedback.inc',
        'weight' => 1,
    );

    $items['cms/response/%/add'] = array(
        'page callback' => 'feedback_response',
        'page arguments' => array(2),
        'type' => MENU_CALLBACK,
        'access arguments' => array('access content'),
        'file' => 'cms_feedback.inc',
    );

    $items['cms/response'] = array(
        'page callback' => 'feedback_response_form_submit_end',
        'type' => MENU_CALLBACK,
        'access arguments' => array('access content'),
        'file' => 'cms_feedback.inc',
    );