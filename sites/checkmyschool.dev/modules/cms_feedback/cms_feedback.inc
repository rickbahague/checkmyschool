<?php

function feedback_summary(){


  $html = "<p>No summary generated yet</p>";
  //drupal_set_message("No summary generated yet.");

  return $html;

}

function feedback_send_submit(){

  $html = '';

  if($_POST['feedback']!=''){
    $data = $_POST;
    $data['created'] = time();
    db_insert('cms_feedback_feedback')->fields($data)->execute();
    $html .= 'Feedback received.';
  }else{
    $html .= 'Empty feedback submission.';
  }

  echo $html;
}

function feedback_send(){

  $html = '';

  return $html;
}

function cms_feedback_tracker(){

  $html = "";
  return $html;
}

function feedback_view(){
  $nid_array = explode('/',$_POST['path']);  
  $nid = $nid_array[3];
  $view = $nid_array[5];

  $html = "";
  if($view == 'new'){
    $html .= views_embed_view('views_feedbacks','block_feedback_content',$nid);
  }elseif($view == 'responded'){
    $html .= views_embed_view('views_feedbacks','block_respond_content',$nid);
  }elseif($view == 'resolved'){
    $html .= views_embed_view('views_feedbacks','block_resolved_content',$nid);
  }
  print $html;
}


function feedback_front($view = null){

  //ctools_include('modal');
  //ctools_modal_add_js();

  $title = $_GET['q'];
  if($title == 'cms/feedback/new/tracker'){
    $title = 'SEND FEEDBACK';
  }elseif($title == 'cms/feedback/responded/tracker'){
    $title = 'RESPONDED FEEDBACK';
  }elseif ($title == 'cms/feedback/resolved/tracker') {
    $title = 'RESOLVED FEEDBACK';
  }else{
    $title = 'FEEDBACK TRACKER';
  }

  $html = "";
  $html .= '<h1 class="title" id="page-title">'.$title.'</h1>';

  $html .= '<ul id="tab-fedback">
    <li class="tab-fedback-item"><a href="/cms/feedback/new/tracker">FEEDBACK</a></li>
    <li class="tab-fedback-item"><a href="/cms/feedback/responded/tracker">RESPONDED</a></li>
    <li class="tab-fedback-item"><a href="/cms/feedback/resolved/tracker">RESOLVED</a></li>
  </ul>';

  $html .= '<div id="feedback-content">';

  if($view == 'new'){
     $query = "select max(nid) as nid from node where type = 'feedback' and status = '1'";
     $result = db_query($query)->fetchField();
     $html .= views_embed_view('views_feedbacks','block_feedback_content',$result);   
  }elseif($view == 'responded'){
     $query = "select field_response_feedback_id_target_id as sequence from field_data_field_response_type,field_data_field_response_feedback_id where field_data_field_response_type.entity_id = field_data_field_response_feedback_id.entity_id and field_response_type_value = :RESPONSE order by field_data_field_response_feedback_id.revision_id desc limit 1 ";
    $result = db_query($query,array(':RESPONSE' => 'RESPONDED'))->fetchField();
    $html .= views_embed_view('views_feedbacks','block_feedback_content',$result);   
  }elseif($view == 'resolved'){
     $query = "select field_response_feedback_id_target_id as sequence from field_data_field_response_type,field_data_field_response_feedback_id where field_data_field_response_type.entity_id = field_data_field_response_feedback_id.entity_id and field_response_type_value = :RESPONSE order by field_data_field_response_feedback_id.revision_id desc limit 1";
    $result = db_query($query,array(':RESPONSE' => 'RESOLVED'))->fetchField();
    $html .= views_embed_view('views_feedbacks','block_feedback_content',$result);  
  }

/*
REF:
select field_data_field_response_feedback_id.revision_id as sequence,field_data_field_response_feedback_id.field_response_feedback_id_target_id as fid, field_response_type_value from 
field_data_field_response_type,
field_data_field_response_feedback_id
where
field_data_field_response_type.entity_id = 
field_data_field_response_feedback_id.entity_id;
*/ 


  $html .= '</div>';
  $html .= '<div id="feedback-slider">';
  if($view == 'new'){
    $html .= views_embed_view('views_feedbacks','views_feedbacks_block');
  }elseif($view == 'responded'){
    $html .= views_embed_view('views_feedbacks','views_responded_block');
  }elseif($view == 'resolved'){
    $html .= views_embed_view('views_feedbacks','views_resolved_block');
  }
  $html .= '</div>';
  return $html;
}


function recent_survey(){

  $query = "select max(node.nid) as nid from node, webform where node.status = '1' and webform.nid = node.nid";
  $result = db_query($query)->fetchField();
  $path = drupal_lookup_path('alias','node/'.$result);
  drupal_goto($path);
}


function feedback_title($title){
  $title = "View Feedbacks";
  return $title;
}

function feedback_response_form_submit_end(){

  $html = '<div id="respond-message">';
  $html .= 'Response recorded. Please close this box to return to the main site.';
  $html .= '</div>';
  print $html;

}


function feedback_response_form_submit($form,&$form_state){

  $values = $form_state['values'];

  $node = new stdClass();
  $node->type = 'response';
  node_object_prepare($node);

  $node->title    = $values['title'];
  $node->language = LANGUAGE_NONE;

  $node->body[$node->language][0]['value']   = $values['body'];
  $node->body[$node->language][0]['summary'] = text_summary($values['body']);
  $node->body[$node->language][0]['format']  = 'filtered_html';

  $node->field_response_type[$node->language][0]['value'] = $values['field_response_type'];
  //$node->field_response_type[$node->language][0]['format'] = ;

  $node->field_response_feedback_id['und'][0]['target_id'] = $values['field_response_feedback_id'];
  //$node->field_response_feedback_id[$node->language][0]['format'] = ;

  node_save($node);

  drupal_goto('cms/response');
}

function feedback_response($feedback_id){
  global $user;
  if($user->login){
    $elements = drupal_get_form("feedback_response_form",$feedback_id); 
    $form = drupal_render($elements);
    echo $form;
  }else{
    echo "Please login to send a response.";
  }

}


function feedback_response_form($form,&$form_state,$feedback_id = null){

  $form = array();
 
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Response Title'),
  );

  $form['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Response'),
  );
 
  $form['field_response_type'] = array(
    '#type' => 'select',
    '#title' => t('Response text'),
    '#options' => array(t('') => t('-- Please select type --'), t('RESPONDED') => t('RESPONDED'), t('RESOLVED') => t('RESOLVED')),
    '#empty' => array('')
  );

  $form['field_response_feedback_id'] = array(
    '#type' => 'hidden',
    '#default_value' => $feedback_id,
  );


  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
 
  return $form;

}