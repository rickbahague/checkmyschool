(function( $ ) {

   	function setNavigation(){
		var path = window.location.pathname;
		path = path.replace(/\/$/,"");
		path = decodeURIComponent(path);

		$("#tab-fedback .tab-fedback-item a").each(function (){
			var href = $(this).attr('href');

			if(path.substring(0,href.length) == href){
				//$("#tab-fedback li.tab-fedback-item a.active").css("background","yellow");
				$(this).closest('li').addClass('active');
			}
		});

		if(path == '/cms/feedback/responded/tracker'){
			$("#right-canvass a").hide();
			console.log(path);
		}

		if(path == '/cms/feedback/resolved/tracker'){
			$("#right-canvass a").hide();
			console.log(path);
		}

		if(path == '/feedback/send'){
			$("#edit-field-feedback-mobile-num").hide();
			console.log(path);
		}

	}

    $(document).ready(function(){

    	setNavigation();

    	$(".Views-Feedbacks-Block ul li .views-field a").click(function(eventClickResponse){
    		var href = $(this).attr('href');
    		eventClickResponse.preventDefault();
    		console.log(href);

			jQuery.ajax({
				type:"post", 
				url: "cms/feedback/view", 
				data:"path="+href, 
				success: function(data) {
					$("#feedback-content").html("");
					$("#feedback-content").html(data);
				}
			});

    	})
	  
	})


})( jQuery );