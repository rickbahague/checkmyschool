<?php
/**
 * @file
 * homepage_feature.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function homepage_feature_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'home_page_context';
  $context->description = 'Home-Page-Context';
  $context->tag = 'Static-Pages';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-frontpage_anon_context-block' => array(
          'module' => 'views',
          'delta' => 'frontpage_anon_context-block',
          'region' => 'content',
          'weight' => '-10',
        ),
        'boxes-frontpage_header_menu' => array(
          'module' => 'boxes',
          'delta' => 'frontpage_header_menu',
          'region' => 'header_first',
          'weight' => '-10',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'home_page_template',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Home-Page-Context');
  t('Static-Pages');
  $export['home_page_context'] = $context;

  return $export;
}
