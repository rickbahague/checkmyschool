<?php
/**
 * @file
 * homepage_feature.box.inc
 */

/**
 * Implements hook_default_box().
 */
function homepage_feature_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'frontpage_header_menu';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Box-Frontpage-Header-Menu';
  $box->options = array(
    'body' => array(
      'value' => '<ul><li><a href="content/2013/11/07/be-volunteer"><img src="/sites/checkmyschool.dev/files/site-images/header-menu-bevol.png"></a></li><li><a href="feedback/send"><img src="/sites/checkmyschool.dev/files/site-images/header-menu-feedb.png"></a></li><li><a href="cms/feedback/new/tracker"><img src="/sites/checkmyschool.dev/files/site-images/header-menu-solve.png"></a></li><li><a href="search/node"><img src="/sites/checkmyschool.dev/files/site-images/header-menu-srch.png"></a></li></ul>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['frontpage_header_menu'] = $box;

  return $export;
}
