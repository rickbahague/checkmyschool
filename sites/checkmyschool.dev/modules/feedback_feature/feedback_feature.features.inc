<?php
/**
 * @file
 * feedback_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feedback_feature_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "boxes" && $api == "box") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function feedback_feature_node_info() {
  $items = array(
    'feedback' => array(
      'name' => t('Feedback'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Feedback Title'),
      'help' => '',
    ),
    'webform' => array(
      'name' => t('Webform'),
      'base' => 'node_content',
      'description' => t('Create a new form or questionnaire accessible to users. Submission results and statistics are recorded and accessible to privileged users.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
