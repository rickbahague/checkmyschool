<?php
/**
 * @file
 * feedback_feature.box.inc
 */

/**
 * Implements hook_default_box().
 */
function feedback_feature_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'box_create_feedback_page_right';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Box-Create-Feedback-Page-Right';
  $box->options = array(
    'body' => array(
      'value' => '<?php
$path = $_GET[\'q\'];
$html = \'<ul>\';
$url = $_SERVER[\'REQUEST_URI\'];
$path2 = explode(\'/\',$url);
if($path == \'feedback/send\'){
  $html .= \'<li><a href="/cms/feedback/front"><img src="/sites/checkmyschool.dev/files/site-images/feedback-page-seefeedback.png"></a></li><li>\';
  $html .= \'<a href="/cms/feedback/survey"><img src="/sites/checkmyschool.dev/files/site-images/feedback-page-survey.png"></a></li>\';
}
if($path == \'feedback/view\'){
  $html .= \'<a href="/feedback/send"><img src="/sites/checkmyschool.dev/files/site-images/feedback-page-send.png"></a></li>\';
   $html .= \'<a href="/cms/feedback/survey"><img src="/sites/checkmyschool.dev/files/site-images/feedback-page-survey.png"></a></li>\';

}
if($path2[1] == \'webform\'){
  $html .= \'<a href="/feedback/send"><img src="/sites/checkmyschool.dev/files/site-images/feedback-page-send.png"></a></li>\';
  $html .= \'<li><a href="/cms/feedback/front"><img src="/sites/checkmyschool.dev/files/site-images/feedback-page-seefeedback.png"></a></li><li>\';
}

$html .= \'</ul>\';
print $html;

?>',
      'format' => 'php_code',
    ),
    'additional_classes' => 'Box-Create-Feedback-Page-Right',
  );
  $export['box_create_feedback_page_right'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'box_feedback_page';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Box-Feedback-Page';
  $box->options = array(
    'body' => array(
      'value' => '<ul><li><img src="/sites/checkmyschool.dev/files/site-images/feedback-send-button.png"></li><li><img src="/sites/checkmyschool.dev/files/site-images/feedback-see-button.png"></li><li><img src="/sites/checkmyschool.dev/files/site-images/feedback-survey-button.png"></li></ul>',
      'format' => 'full_html',
    ),
    'additional_classes' => 'Box-Feedback-Page',
  );
  $export['box_feedback_page'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'box_send_feedback_right';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Box-Send-Feedback-Right';
  $box->options = array(
    'body' => array(
      'value' => '<p><br><br><strong><span style="font-size:16px;">Text CMS</span></strong></p><p><span style="font-size:16px;">CMS &lt;message&gt; send to 2948</span></p><p><span style="font-size:16px;">or</span></p><p><span style="font-size:16px;">&lt;message&gt; &lt;name of school&gt; and send to xxxx</span></p><p>&nbsp;</p><p><span style="font-size:12px;">*Only message sent through either of these two are consolidated and forwarded to DepEd and other sectors.</span></p>',
      'format' => 'full_html',
    ),
    'additional_classes' => 'Box-Send-Feedback-Right',
  );
  $export['box_send_feedback_right'] = $box;

  return $export;
}
