<?php
/**
 * @file
 * feedback_feature.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function feedback_feature_taxonomy_default_vocabularies() {
  return array(
    'feedback' => array(
      'name' => 'Feedback',
      'machine_name' => 'feedback',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
