<?php
/**
 * @file
 * feedback_feature.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function feedback_feature_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feedack_summary_page_context';
  $context->description = 'Feedack-Summary-Page-Context';
  $context->tag = 'Feedback-Context';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'cms/feedback/summary' => 'cms/feedback/summary',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'boxes-box_feedback_page' => array(
          'module' => 'boxes',
          'delta' => 'box_feedback_page',
          'region' => 'content',
          'weight' => '-57',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-56',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'home_page_template',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feedack-Summary-Page-Context');
  t('Feedback-Context');
  $export['feedack_summary_page_context'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'send_feedback_context';
  $context->description = 'Send-Feedback-Context';
  $context->tag = 'Feedback-Context';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'feedback/send' => 'feedback/send',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-9',
        ),
        'cms-embed-feedback-form' => array(
          'module' => 'cms',
          'delta' => 'embed-feedback-form',
          'region' => 'content',
          'weight' => '-8',
        ),
        'boxes-box_create_feedback_page_right' => array(
          'module' => 'boxes',
          'delta' => 'box_create_feedback_page_right',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'boxes-box_send_feedback_right' => array(
          'module' => 'boxes',
          'delta' => 'box_send_feedback_right',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'clone_of_2013_node_template',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feedback-Context');
  t('Send-Feedback-Context');
  $export['send_feedback_context'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'survey_context';
  $context->description = 'Survey-Context';
  $context->tag = 'Feedback-Context';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'webform' => 'webform',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'node/*/done*' => 'node/*/done*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-9',
        ),
        'boxes-box_create_feedback_page_right' => array(
          'module' => 'boxes',
          'delta' => 'box_create_feedback_page_right',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'cms-survey-results' => array(
          'module' => 'cms',
          'delta' => 'survey-results',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => '2013_survey_template',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feedback-Context');
  t('Survey-Context');
  $export['survey_context'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'survey_results_context';
  $context->description = 'Survey-Context';
  $context->tag = 'Feedback-Context';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/*/done*' => 'node/*/done*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-9',
        ),
        'boxes-box_create_feedback_page_right' => array(
          'module' => 'boxes',
          'delta' => 'box_create_feedback_page_right',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'cms-survey-results' => array(
          'module' => 'cms',
          'delta' => 'survey-results',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'survey_template',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feedback-Context');
  t('Survey-Context');
  $export['survey_results_context'] = $context;

  return $export;
}
