//jQuery(document).ready(function(){

    var region="";
    var division="";
    var district="";
    var markersArray=[];
    var marker;
    var map;
    var bounds;
    
    function getPointsByRegion(region_name){
		var str = jQuery.ajax({                                  
			url: '/school/map/getPointsByRegion.php/region_name='+region_name,
			async: false,
			data: {'region_name':region_name},
			dataType: 'json',
			type: 'POST',
	    }).responseText;
		return str;
	}
	
	function getPointsByDivision(region_name, division_name){
		var str = jQuery.ajax({                                  
			url: '/school/map/getPointsByDivision.php/region_name='+region_name+'/division_name='+division_name,
			async: false,
			data: {'region_name':region_name, 'division_name':division_name},
			dataType: 'json',
			type: 'POST',
	    }).responseText;
		return str;
	}
	
	function getPointsByDistrict(region_name, division_name, district_name){
		var str = jQuery.ajax({                                  
			url: '/school/map/getPointsByDistrict.php/region_name='+region_name+'/division_name='+division_name+'/district_name='+district_name,
			async: false,
			data: {'region_name':region_name, 'division_name':division_name, 'district_name':district_name},
			dataType: 'json',
			type: 'POST',
	    }).responseText;
		return str;
	}
	
	function getDistrictDetail(lat, lon){
		var str = jQuery.ajax({                                  
			url: '/school/map/getDistrictDetail.php/lat='+lat+'/lon='+lon,
			async: false,
			data: {'lat':lat, 'lon':lon},
			dataType: 'json',
			type: 'POST',
	    }).responseText;
		return str;
	}
	
	function getDivisionDetail(lat, lon){
		var str = $.ajax({                                  
			url: '/school/map/getDivisionDetail.php/lat='+lat+'/lon='+lon,
			async: false,
			data: {'lat':lat, 'lon':lon},
			dataType: 'json',
			type: 'POST',
	    }).responseText;
		return str;
	}
    
    function setMarkersByRegion(points, region){
			for (var i=0; i<points.length; i++) {
				var latlngset;
            	latlngset = new google.maps.LatLng(parseFloat(points[i].lat), parseFloat(points[i].lon))
            	
            	marker = new google.maps.Marker({   
              		position: latlngset
            	});
            	markersArray.push(marker);
            	
            	var contents = getDivisionDetail(points[i].lat, points[i].lon);;
				contents = JSON.parse(contents);
				
				for(var j=0; j<contents.length; j++){
					var div = contents[j].division;
					var dist = contents[j].district;
            	}
            	
            	var content='<div id="content" style="width: 350px; height:250px;">'+
            		'<p>School:' + points[i].school +
            		'</p><p>School ID:' + points[i].id +
            		'</p><p>Region:' + points[i].region +
            		'</p><p>Division:' + div +
            		'</p><p>District:' + dist +
            		'</p><p>Latitude:' + points[i].lat +
            		'</p><p>Longitude:' + points[i].lon +
            		'</p></div>'
            	;
            	
            	var infowindow = new google.maps.InfoWindow();
				infowindow.setContent(content);
        
		        google.maps.event.addListener(
		        	marker, 
		            'click', 
		            infoCallback(infowindow, marker)
		        );
           
			}
			
			console.log(markersArray.length);
			
			for (i in markersArray) {
    		  markersArray[i].setMap(map);
    		}
    		
		}
		
		function setMarkersByDivision(points, region, division){
			for (var i=0; i<points.length; i++) {
				var latlngset;
            	latlngset = new google.maps.LatLng(parseFloat(points[i].lat), parseFloat(points[i].lon))
            	
            	marker = new google.maps.Marker({   
              		position: latlngset
            	});
            	markersArray.push(marker);
            
            	var contents = getDivisionDetail(points[i].lat, points[i].lon);;
				contents = JSON.parse(contents);
				
				var dist = contents[0].district;
				
            	var content='<div id="content" style="width: 350px; height:250px;">'+
            		'<p>School:' + points[i].school +
            		'</p><p>School ID:' + points[i].id +
            		'</p><p>Region:' + points[i].region +
            		'</p><p>Division:' + points[i].division +
            		'</p><p>District:' + dist +
            		'</p><p>Latitude:' + points[i].lat +
            		'</p><p>Longitude:' + points[i].lon +
            		'</p></div>'
            	;
            	
            	var infowindow = new google.maps.InfoWindow();
				infowindow.setContent(content);
        
		        google.maps.event.addListener(
		        	marker, 
		            'click', 
		            infoCallback(infowindow, marker)
		        );
			}
			console.log(markersArray.length);
			for (i in markersArray) {
    		  markersArray[i].setMap(map);
    		}
		}
		
		function setMarkersByDistrict(points, region, division, district){
			for (var i=0; i<points.length; i++) {
				var latlngset;
            	latlngset = new google.maps.LatLng(parseFloat(points[i].lat), parseFloat(points[i].lon))
            	
            	marker = new google.maps.Marker({   
              		position: latlngset
            	});
            	markersArray.push(marker);
            	            	
            	var content='<div id="content" style="width: 350px; height:250px;">'+
            		'<p>School:' + points[i].school +
            		'</p><p>School ID:' + points[i].id +
            		'</p><p>Region:' + points[i].region +
            		'</p><p>Division:' + points[i].division +
            		'</p><p>District:' + points[i].district +
            		'</p><p>Latitude:' + points[i].lat +
            		'</p><p>Longitude:' + points[i].lon +
            		'</p></div>'
            	;
            	
            	var infowindow = new google.maps.InfoWindow();
				infowindow.setContent(content);
        
		        google.maps.event.addListener(
		        	marker, 
		            'click', 
		            infoCallback(infowindow, marker)
		        );
			}
			console.log(markersArray.length);
			for (i in markersArray) {
    		  markersArray[i].setMap(map);
    		}
		}
    
    function infoCallback(infowindow, marker) { 
            return function() {
            	infowindow.open(map, marker);
        	};
        }
        
	function deleteMarker(){
		for (i in markersArray) {
			markersArray[i].setMap(null);
		}
		markersArray.length = 0;
	}
    
    function initialize() {

        var mapOptions = {
          center: new google.maps.LatLng(12.1667,121.5833),
          zoom: 6,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);
    }
      
      google.maps.event.addDomListener(window, 'load', initialize);


      $(document).ready(function(){
		
		$("#region").change(function(){
			region = $("#region").val();
				jQuery.ajax({
				type:"post", 
				url:"/school/map/getDivision.php", 
				data:"region="+region, 
				success: function(data) {
					$("#division").html(data);
				}
			});
			
			division = $("#division").val();
				jQuery.ajax({
				type:"post", 
				url:"/school/map/getDistrict1.php", 
				data:"division="+division, 
				success: function(data) {
					$("#district").html(data);
				}
			});

		});
		
		$("#division").change(function(){
			division = $("#division").val();
				jQuery.ajax({
				type:"post", 
				url:"/school/map/getDistrict.php", 
				data:"division="+division, 
				success: function(data) {
					$("#district").html(data);
				}
			});
		});
		
		$("#district").change(function(){
			district = $("#district").val();
		});
		
		$('form[name="thisform"]').submit(function(event){
			event.preventDefault();

			region = $("#region").val();
			division = $("#division").val();
			district = $("#district").val();

			if(region!='' && division=='' && district==''){

				deleteMarker();

				var points = getPointsByRegion(region);

				points = JSON.parse(points);
				
				if(points!=''){
					setMarkersByRegion(points, region);
				} else{
					deleteMarker();
					console.log("No school data available");
				}

			}else if(region!='' && division!='' && district==''){

				deleteMarker();
				
				var points = getPointsByDivision(region, division);

				points = JSON.parse(points);
				
				if(points!=''){
					setMarkersByDivision(points, region, division);
				} else{
					deleteMarker();
					console.log("No school data available"); 
				}
					
			}else if(region!='' && division!='' && district!=''){

				deleteMarker();

				var points = getPointsByDistrict(region, division, district);

				points = JSON.parse(points);
				
				if(points!=''){
					setMarkersByDistrict(points, region, division, district);
				} else{
					deleteMarker();
					console.log("No school data available");
				}
			} else{
				for (i in markersArray) {
			      	markersArray[i].setMap(null);
			    }
			    markersArray.length = 0;
			}
		});
	});
