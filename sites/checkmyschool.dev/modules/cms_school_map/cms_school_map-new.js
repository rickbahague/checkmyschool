   var region="";
    var division="";
    var district="";
    var markersArray=[];
    var marker;
    var map;
    var bounds = new google.maps.LatLngBounds();
    
    function getPointsByRegion(region_name){
		var str = $.ajax({                                  
			url: '/school/map/getPointsByRegion.php/region_name='+region_name,
			async: false,
			data: {'region_name':region_name},
			dataType: 'json',
			type: 'POST',
	    }).responseText;
		return str;
	}
	
	function getPointsByDivision(region_name, division_name){
		var str = $.ajax({                                  
			url: '/school/map/getPointsByDivision.php/region_name='+region_name+'/division_name='+division_name,
			async: false,
			data: {'region_name':region_name, 'division_name':division_name},
			dataType: 'json',
			type: 'POST',
	    }).responseText;
		return str;
	}
	
	function getPointsByDistrict(region_name, division_name, district_name){
		var str = $.ajax({                                  
			url: '/school/map/getPointsByDistrict.php/region_name='+region_name+'/division_name='+division_name+'/district_name='+district_name,
			async: false,
			data: {'region_name':region_name, 'division_name':division_name, 'district_name':district_name},
			dataType: 'json',
			type: 'POST',
	    }).responseText;
		return str;
	}
	
    function setMarkersByRegion(points, region){
			for (var i=0; i<points.length; i++) {
				var latlngset;
            	latlngset = new google.maps.LatLng(parseFloat(points[i].lat), parseFloat(points[i].lon))
            	
            	marker = new google.maps.Marker({   
              		position: latlngset
            	});
            	markersArray.push(marker);
            	
            	var content='<div id="content" style="width: 350px; height:250px;">'+
            		'<p>School: <a href="/school/'+points[i].nid +'">' + points[i].school +
            		'</a></p><p>School ID:' + points[i].id +
            	//	'</p><p>Barangay:' + points[i].barangay +
            		'</p><p>District:' + points[i].district +
            		'</p><p>Division:' + points[i].division +
            		'</p><p>Region:' + points[i].region +
            		'</p><p>Latitude:' + points[i].lat +
            		'</p><p>Longitude:' + points[i].lon +
            		'</p></div>';
            	
            	var infowindow = new google.maps.InfoWindow();
				infowindow.setContent(content);
        
		        google.maps.event.addListener(
		        	marker, 
		            'click', 
		            infoCallback(infowindow, marker)
		        );
			}
			
			for (i in markersArray) {
    		  markersArray[i].setMap(map);
    		}
		}
		
		function setMarkersByDivision(points, region, division){
			for (var i=0; i<points.length; i++) {
				var latlngset;
            	latlngset = new google.maps.LatLng(parseFloat(points[i].lat), parseFloat(points[i].lon))
            	
            	marker = new google.maps.Marker({   
              		position: latlngset
            	});
            	markersArray.push(marker);
           
           		var content='<div id="content" style="width: 350px; height:250px;">'+
            		'<p>School: <a href="/school/'+points[i].nid +'">' + points[i].school +
            		'</a></p><p>School ID:' + points[i].id +
            	//	'</p><p>Barangay:' + points[i].barangay +
            		'</p><p>District:' + points[i].district +
            		'</p><p>Division:' + points[i].division +
            		'</p><p>Region:' + points[i].region +
            		'</p><p>Latitude:' + points[i].lat +
            		'</p><p>Longitude:' + points[i].lon +
            		'</p></div>';
            	
            	var infowindow = new google.maps.InfoWindow();
				infowindow.setContent(content);
        
		        google.maps.event.addListener(
		        	marker, 
		            'click', 
		            infoCallback(infowindow, marker)
		        );
			}
			
			for (i in markersArray) {
    		  markersArray[i].setMap(map);
    		}
		}
		
		function setMarkersByDistrict(points, region, division, district){
			for (var i=0; i<points.length; i++) {
				var latlngset;
            	latlngset = new google.maps.LatLng(parseFloat(points[i].lat), parseFloat(points[i].lon))
            	
            	marker = new google.maps.Marker({   
              		position: latlngset
            	});
            	markersArray.push(marker);
            	            	
            	var content='<div id="content" style="width: 350px; height:250px;">'+
            		'<p>School: <a href="/school/'+points[i].nid +'">' + points[i].school +
            		'</a></p><p>School ID:' + points[i].id +
            	//	'</p><p>Barangay:' + points[i].barangay +
            		'</p><p>District:' + points[i].district +
            		'</p><p>Division:' + points[i].division +
            		'</p><p>Region:' + points[i].region +
            		'</p><p>Latitude:' + points[i].lat +
            		'</p><p>Longitude:' + points[i].lon +
            		'</p></div>';
            	
            	var infowindow = new google.maps.InfoWindow();
				infowindow.setContent(content);
        
		        google.maps.event.addListener(
		        	marker, 
		            'click', 
		            infoCallback(infowindow, marker)
		        );
			}
			
			console.log(markersArray.length);
			for (i in markersArray) {
    		  markersArray[i].setMap(map);
    		}
		}
    
    function infoCallback(infowindow, marker) { 
            return function() {
            	infowindow.open(map, marker);
        	};
        }
        
	function deleteMarker(){
		for (i in markersArray) {
			markersArray[i].setMap(null);
		}
		markersArray.length = 0;
	}
    
      function initialize() {
        var mapOptions = {
          center: new google.maps.LatLng(14.583333, 121.966667),
          zoom: 6,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        
        map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);
      }
      
      google.maps.event.addDomListener(window, 'load', initialize);
      	
      $(document).ready(function(){
      	$("#region").change(function(){
			region = $("#region").val();
			$.ajax({
				type:"post", 
				url:"getDivision.php", 
				data:"region="+region, 
				success: function(data) {
					$("#division").html(data);
				}
			});
			
			division = $("#division").val();
			$.ajax({
				type:"post", 
				url:"getDistrict1.php", 
				data:"division="+division, 
				success: function(data) {
					$("#district").html(data);
				}
			});
		});
		
		$("#division").change(function(){
			division = $("#division").val();
			$.ajax({
				type:"post", 
				url:"getDistrict.php", 
				data:"division="+division, 
				success: function(data) {
					$("#district").html(data);
				}
			});
		});
		
		$("#district").change(function(){
			district = $("#district").val();
		});
		
		$('form[name="thisform"]').submit(function(event){
			event.preventDefault();
			
			region = $("#region").val();
			division = $("#division").val();
			district = $("#district").val();
			
			if(region!='' && division=='' && district==''){
				deleteMarker();
				
				var points = getPointsByRegion(region);
				points = JSON.parse(points);
				
				if(points!=''){
				    setMarkersByRegion(points, region);
					
					$("span.note").text('There are ' + markersArray.length + ' schools available');
					$("a.searchLink").html("<a href=/search/node/"+region+">Click here</a>");
				} else{
					deleteMarker();
					$("span.note").text('No school data available');
				}
				
			}else if(region!='' && division!='' && district==''){
				deleteMarker();
				
				var points = getPointsByDivision(region, division);
				points = JSON.parse(points);
				
				if(points!=''){
					setMarkersByDivision(points, region, division);
					
					$("span.note").text('There are ' + markersArray.length + ' schools available');
					
					var url = escape(region+' '+division);
					$("a.searchLink").html("<a href=/search/node/"+url+">Click Here</a>");
				} else{
					deleteMarker();
					$("span.note").text('No school data available');
				}
					
			}else if(region!='' && division!='' && district!=''){
				deleteMarker();
				
				var points = getPointsByDistrict(region, division, district);
				points = JSON.parse(points);
				
				if(points!=''){
					setMarkersByDistrict(points, region, division, district);
					
					$("span.note").text('There are ' + markersArray.length + ' schools available');
					
					var url = escape(region+' '+division+' '+district);
					$("a.searchLink").html("<a href=/search/node/"+url+">Click Here</a>");
				} else{
					deleteMarker();
					$("span.note").text('No school data available');
				}
			} else{
				for (i in markersArray) {
			      	markersArray[i].setMap(null);
			    }
			    markersArray.length = 0;
			}
		});
	});