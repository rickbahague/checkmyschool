<?php

$path = drupal_get_path('module','cms_school_map');

$html .= '

    <form id="form-map-school" name="thisform">

	<div id="map-instructions">
	To find your school, use filters and click the <em>OK</em> button.	
	Each red bubble represents a school which can be clicked to visit the corresponding school page. You can also zoom in or out on the map. 
	</div>

    	<br/>
    	<select name="region" id="region"> ';
    		
				$query = "SELECT region_name FROM cms_map_region " ;
				$result = db_query($query) or die(mysql_error());
				
				$html .="<option value=''>--Select Region--</option>";

				foreach($result as $row){
					$html .= "<option value='".$row->region_name."'> ".$row->region_name." </option> ";	
				}

$html .='

		</select>
		<br/><br/>
    	<select id="division" name="division">
			<option value="">--Select Division--</option>
			
		</select>
		<br/><br/>
		<select id="district" name="district">
			<option value="">--Select District--</option>
		</select><br/>
		<br/><br/>
		<input id ="submit-map-query" type="submit" name="submit" value="OK">

	<div id="map-instructions-image">

	<a href="/school/map/help"><img width="340px" src="/'.$path.'/School-Map.png"></a>

	</div>
	


    </form>
    
    <div id="map-canvas"><br><br>';
?>
