HOME {"url":"welcome"}
Map {"url":"<front>","hidden":true}
Archive {"url":"<front>","hidden":true}
Community {"url":"<front>","hidden":true}
ABOUT US {"url":"content\/about-checkmyschool"}
-SECRETARIAT {"url":"content\/2013\/08\/12\/cms-national-team"}
-AREA COORDINATORS {"url":"<front>"}
-COVERAGE {"url":"content\/2013\/08\/12\/cms-area-coverage","expanded":true}
-CONTACT US {"url":"contact"}
LEARNING {"url":"learning"}
-FAQ {"url":"content\/2013\/08\/12\/faq"}
-FORUMS {"url":"forums\/checkmyschool-forum"}
SCHOOL INFORMATION {"url":"search"}
-SCHOOL MAP {"url":"school\/map","description":"Map of schools"}
-SCHOOL PAGES {"url":"search\/node"}
-DATA SUMMARY {"url":"summary\/data\/national","description":"Data Summary"}
-DATA VISUALIZATION {"url":"school\/data\/visualize","description":"Visualization of school data"}
-ARCHIVES {"url":"archive","hidden":true}
-PHOTOS {"url":"gallery\/photos"}
-VIDEOS {"url":"gallery\/videos"}
NETWORK {"url":"<front>"}
-VOLUNTEERS {"url":"cms\/volunteers\/121","description":"Volunteers"}
-PARTNERS {"url":"partners"}
-CMS AWARDEES {"url":"cms-heroes"}
NEWS {"url":"news"}
-ANNOUNCEMENTS {"url":"articles"}
-BLOGS {"url":"articles","hidden":true}
-NEWS UPDATES {"url":"news"}
My Account {"url":"user","hidden":true}
Logout {"url":"user\/logout","hidden":true}
 {"url":"gallery","description":"Gallery","hidden":true}
-Videos {"url":"gallery\/videos","hidden":true}
