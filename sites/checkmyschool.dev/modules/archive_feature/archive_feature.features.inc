<?php
/**
 * @file
 * archive_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function archive_feature_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "boxes" && $api == "box") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "quicktabs" && $api == "quicktabs") {
    return array("version" => "1");
  }
}
