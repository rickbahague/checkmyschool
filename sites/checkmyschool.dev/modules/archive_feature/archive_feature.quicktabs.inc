<?php
/**
 * @file
 * archive_feature.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function archive_feature_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'archive';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Archive';
  $quicktabs->tabs = array(
    0 => array(
      'machine_name' => 'school_data',
      'title' => 'Elementary Data',
      'weight' => '-100',
      'type' => 'qtabs',
    ),
    1 => array(
      'machine_name' => 'high_school_data',
      'title' => 'High School Data',
      'weight' => '-99',
      'type' => 'qtabs',
    ),
    2 => array(
      'machine_name' => 'regional_data',
      'title' => 'Regional Data',
      'weight' => '-98',
      'type' => 'qtabs',
    ),
    3 => array(
      'machine_name' => 'miscellaneous',
      'title' => 'Miscellaneous',
      'weight' => '-97',
      'type' => 'qtabs',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'Zen';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Archive');
  t('Elementary Data');
  t('High School Data');
  t('Miscellaneous');
  t('Regional Data');

  $export['archive'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'high_school_data';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 5;
  $quicktabs->title = 'High School Data';
  $quicktabs->tabs = array(
    0 => array(
      'nid' => '46864',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2005',
      'weight' => '-100',
      'type' => 'node',
    ),
    1 => array(
      'nid' => '46866',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2006',
      'weight' => '-99',
      'type' => 'node',
    ),
    2 => array(
      'nid' => '46868',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2007',
      'weight' => '-98',
      'type' => 'node',
    ),
    3 => array(
      'nid' => '34489',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2008',
      'weight' => '-97',
      'type' => 'node',
    ),
    4 => array(
      'nid' => '34490',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2009',
      'weight' => '-96',
      'type' => 'node',
    ),
    5 => array(
      'nid' => '34566',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2010',
      'weight' => '-95',
      'type' => 'node',
    ),
    6 => array(
      'nid' => '34568',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2011',
      'weight' => '-94',
      'type' => 'node',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'Zen';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('2005');
  t('2006');
  t('2007');
  t('2008');
  t('2009');
  t('2010');
  t('2011');
  t('High School Data');

  $export['high_school_data'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'miscellaneous';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = TRUE;
  $quicktabs->default_tab = 7;
  $quicktabs->title = 'Miscellaneous';
  $quicktabs->tabs = array(
    0 => array(
      'nid' => '34620',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2002',
      'weight' => '-100',
      'type' => 'node',
    ),
    1 => array(
      'nid' => '34621',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2003',
      'weight' => '-99',
      'type' => 'node',
    ),
    2 => array(
      'nid' => '34622',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2004',
      'weight' => '-98',
      'type' => 'node',
    ),
    3 => array(
      'nid' => '34623',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2005',
      'weight' => '-97',
      'type' => 'node',
    ),
    4 => array(
      'nid' => '34624',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2006',
      'weight' => '-96',
      'type' => 'node',
    ),
    5 => array(
      'nid' => '34625',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2007',
      'weight' => '-95',
      'type' => 'node',
    ),
    6 => array(
      'nid' => '34626',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2008',
      'weight' => '-94',
      'type' => 'node',
    ),
    7 => array(
      'nid' => '34492',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2009',
      'weight' => '-93',
      'type' => 'node',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'Zen';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('2002');
  t('2003');
  t('2004');
  t('2005');
  t('2006');
  t('2007');
  t('2008');
  t('2009');
  t('Miscellaneous');

  $export['miscellaneous'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'regional_data';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Regional Data';
  $quicktabs->tabs = array(
    0 => array(
      'nid' => '34491',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => 'Regional Data',
      'weight' => '-100',
      'type' => 'node',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'Zen';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Regional Data');

  $export['regional_data'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'school_data';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 5;
  $quicktabs->title = 'Elementary School Data';
  $quicktabs->tabs = array(
    0 => array(
      'nid' => '46851',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2005',
      'weight' => '-100',
      'type' => 'node',
    ),
    1 => array(
      'nid' => '46865',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2006',
      'weight' => '-99',
      'type' => 'node',
    ),
    2 => array(
      'nid' => '46867',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2007',
      'weight' => '-98',
      'type' => 'node',
    ),
    3 => array(
      'nid' => '34487',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2008',
      'weight' => '-97',
      'type' => 'node',
    ),
    4 => array(
      'nid' => '34486',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2009',
      'weight' => '-96',
      'type' => 'node',
    ),
    5 => array(
      'nid' => '34565',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2010',
      'weight' => '-95',
      'type' => 'node',
    ),
    6 => array(
      'nid' => '34569',
      'teaser' => 0,
      'hide_title' => 1,
      'title' => '2011',
      'weight' => '-94',
      'type' => 'node',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'Zen';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('2005');
  t('2006');
  t('2007');
  t('2008');
  t('2009');
  t('2010');
  t('2011');
  t('Elementary School Data');

  $export['school_data'] = $quicktabs;

  return $export;
}
