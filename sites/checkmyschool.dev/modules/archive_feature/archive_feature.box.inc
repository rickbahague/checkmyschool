<?php
/**
 * @file
 * archive_feature.box.inc
 */

/**
 * Implements hook_default_box().
 */
function archive_feature_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'box_archive_box';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Box-Archive-Button';
  $box->options = array(
    'body' => array(
      'value' => '<ul><li><a href="/archive?qt-archive=0#qt-archive"><img id="elem-data" src="/sites/checkmyschool.dev/files/site-images/elem-data.png"></a></li><li><a href="/archive?qt-archive=1#qt-archive"><img id="hs-data"  src="/sites/checkmyschool.dev/files/site-images/high-school-data.png"></a></li><li><a href="/archive?qt-archive=2#qt-archive"><img id="region-data"  src="/sites/checkmyschool.dev/files/site-images/region-data.png"></a></li><li><a href="/archive?qt-archive=3#qt-archive"><img id="misc-data"  src="/sites/checkmyschool.dev/files/site-images/misc-data.png"></a></li></ul>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['box_archive_box'] = $box;

  return $export;
}
