 // Disable default ckeditor on some textareas.
  Drupal.behaviors.ckeditor_noautostart = {
    attach: function(context) {
      // Subheading field off
      if ($('div#Feedback-Form-Block').length > 0){
        if (typeof(Drupal.settings.ckeditor.autostart) != 'undefined' && typeof(Drupal.settings.ckeditor.autostart['edit-field-subheading-und-0-value']) != 'undefined') {
          delete Drupal.settings.ckeditor.autostart['edit-body-und-0-value'];
          $('div.field-name-field-subheading a.ckeditor_links').html('Switch to rich-text editor');
        }
      }
    }
  }  